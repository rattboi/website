# kanyid.org

[![Netlify Status](https://api.netlify.com/api/v1/badges/ef25b8b0-b48b-4e06-9707-2dcd9f6c4dcd/deploy-status)](https://app.netlify.com/sites/enchanting-sprinkles-d3a5e8/deploys)

Site content for [kanyid.org](https://kanyid.org)

Static site generator: [Hugo](https://gohugo.io)

Hosting: [Netlify](https://www.netlify.com/)
